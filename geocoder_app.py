from flask import Flask, escape, request, render_template,jsonify,Markup, redirect, url_for
from lib.geocoder.our_geocoder import Geocoder,TextGeocoder
from lib.geocoder.heuristics import *

import spacy

app = Flask(__name__)

dict_model = {
    "FR_AIC":("./outputs/FR_MODEL_2/FR.txt_100_4_100__A_I_C.h5","./outputs/FR_MODEL_2/FR.txt_100_4_100__A_I_C_index"),
    "FR_C":("./outputs/FR_MODEL_2/FR.txt_100_4_100__C.h5","./outputs/FR_MODEL_2/FR.txt_100_4_100__C_index"),
    "FR_AC":("./outputs/FR_MODEL_2/FR.txt_100_4_100__A_C.h5","./outputs/FR_MODEL_2/FR.txt_100_4_100__A_C_index"),
    "FR_IC":("./outputs/FR_MODEL_2/FR.txt_100_4_100__I_C.h5","./outputs/FR_MODEL_2/FR.txt_100_4_100__I_C_index"),
    "FR_AIC_512":("outputs/FR_MODEL_2/nside_512/FR_nside_512/FR_nside_512_4_100_A_I_P.h5.part","outputs/FR_MODEL_2/nside_512/FR_nside_512/FR_nside_512_4_100_A_I_P_index"),

    "GB_AIC":("./outputs/GB_MODEL_2/GB.txt_100_4_100__A_I_C.h5","./outputs/GB_MODEL_2/GB.txt_100_4_100__A_I_C_index"),
    "GB_C":("./outputs/GB_MODEL_2/GB.txt_100_4_100__C.h5","./outputs/GB_MODEL_2/GB.txt_100_4_100__C_index"),
    "GB_AC":("./outputs/GB_MODEL_2/GB.txt_100_4_100__A_C.h5","./outputs/GB_MODEL_2/GB.txt_100_4_100__A_C_index"),
    "GB_IC":("./outputs/GB_MODEL_2/GB.txt_100_4_100__I_C.h5","./outputs/GB_MODEL_2/GB.txt_100_4_100__I_C_index")
    ,"FR_IGN":("./outputs/IGN/onlyAdjac/IGN_4_100_A_C.h5","./outputs/IGN/onlyAdjac/IGN_4_100_A_C_index")
}

MODEL = "FR_AC"
LANG = "fr"
NER = "spacy"

heuristic_func = heuristic_cluster

geocoder = Geocoder(*dict_model[MODEL])
g_t = TextGeocoder(geocoder,NER,LANG,heuristic_func)


@app.route('/')
def home():
    toponym = request.args.get("top", "")
    c_toponym = request.args.get("c_top", "")
    msg = request.args.get("msg", "")
    msg_code = request.args.get("msg_code", "info")
    if toponym and c_toponym:
        lon,lat = geocoder.get_coords([toponym],[c_toponym])
        lon,lat = lon[0],lat[0]
        print(lon,lat)
        return  render_template("pair_topo.html",lat=lat,lon=lon,title="Toponyms Pair Geocoder",dict_model=dict_model,msg_code=msg_code)
    else:
        return  render_template("pair_topo.html",title="Toponyms Pair Geocoder",dict_model=dict_model,msg_code=msg_code)

@app.route('/text')
def text():
    return render_template("text.html",title="Text Geocoder",dict_model=dict_model)

@app.route('/geocode', methods=['POST', 'GET'])
def geocode():
    if request.method == 'POST':
        text = request.form["text"]
        
        results = g_t.geocode(g_t.extract_geo_entities(text))
        
        html_, pos_ = "", 0
        for item in results:
            start,end = item["start"], item["end"]
            html_ = html_ + text[pos_:start] + "<span class=\"annotation place\">{0}</span>".format(text[start:end])
            pos_ = end
        
        place_coords = {}
        for r in results:
            if r["text"] in place_coords:
                continue
            place_coords[r["text"]]={"lat":float(r["coord"]["lat"]),"lon":float(r["coord"]["lon"])}
        return render_template("text.html",title="Text Geocoder",data={"type":"success","output":Markup(html_),"place_coords":place_coords},dict_model=dict_model)


@app.route("/loadmodel/<model_id>")
def loadModel(model_id):
    global geocoder,g_t,LANG
    if not model_id in dict_model:
        return redirect(url_for(".home",msg="An error happend when loading the model \"{0}\"!".format(model_id),msg_code="danger"))
    else: 
        geocoder = Geocoder(*dict_model[model_id])
        g_t = TextGeocoder(geocoder,NER,LANG,heuristic_func)
        return redirect(url_for(".home",msg="Model \"{0}\" was loaded successfuly!".format(model_id),msg_code="success"))

@app.route("/loadlang/<lang>")
def loadLang(lang):
    global geocoder,g_t,LANG
    try:
        g_t = TextGeocoder(geocoder,NER,lang,heuristic_func)
        LANG = lang
        return redirect(url_for(".home",msg="Language is now set to \"{0}\"!".format(LANG),msg_code="success"))
    except:
        return redirect(url_for(".home",msg="\"{}\" language is not available!".format(lang),msg_code="danger"))
        

if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=True)