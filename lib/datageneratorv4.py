import os
from gzip import GzipFile

import keras
from keras.utils import to_categorical
import numpy as np
import pandas as pd

from lib.utils_geo import zero_one_encoding

from helpers import parse_title_wiki,read_geonames
from gensim.models.keyedvectors import KeyedVectors

from sklearn.preprocessing import LabelEncoder

import numpy as np
import keras

class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, pairs_of_toponyms,encoder, batch_size=32, shuffle=True):
        'Initialization'
        self.data= pairs_of_toponyms
        self.encoder = encoder
        self.dim = self.encoder.max_len
        self.shuffle = shuffle
    
        self.batch_size = batch_size
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.data) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Generate data
        return self.__data_generation(indexes)


    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.data))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_ids):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X1 = np.empty((self.batch_size, self.dim))
        X2 = np.empty((self.batch_size, self.dim))
        y = np.zeros((self.batch_size,2), dtype=float)

        # Generate data
        for ix,i in enumerate(list_ids):
            # Store sample
            X1[ix,] = self.encoder.encode(self.data.toponym.iloc[i])
            X2[ix,] = self.encoder.encode(self.data.toponym_context.iloc[i])
            # Store class
            y[ix,] = list(zero_one_encoding(self.data.longitude.iloc[i],self.data.latitude.iloc[i]))

        return [X1,X2],y