import os
import sys
import time
import random
import argparse
import datetime

import pandas as pd
import numpy as np

import tensorflow as tf
import torch

from tqdm import tqdm
tqdm.pandas()

from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from keras.preprocessing.sequence import pad_sequences
from transformers import BertTokenizer
from transformers import BertForSequenceClassification, AdamW, BertConfig
from transformers import get_linear_schedule_with_warmup

from ..torch_generator import SentenceDataset
from ..utils_geo import latlon2healpix,healpix2latlon

import pickle

# If there's a GPU available...
if torch.cuda.is_available():    

    # Tell PyTorch to use the GPU.    
    device = torch.device("cuda")
    print('There are %d GPU(s) available.' % torch.cuda.device_count())
    print('We will use the GPU:', torch.cuda.get_device_name(0))
# If not...
else:
    print('No GPU available, using the CPU instead.')
    device = torch.device("cpu")


class BertGeocoder():
    def __init__(self,bert_model_dir,label_healpix_file,healpix_nside=128,batch_size=1):
        self.bert_model = BertForSequenceClassification.from_pretrained(bert_model_dir)
        self.bert_model.to(device)
        self.tokenizer = BertTokenizer.from_pretrained(bert_model_dir,truncation=True)
        self.label_healpix = {v:k for k, v in pickle.load(open(label_healpix_file,'rb')).items()}

        self.nside = healpix_nside

        self.batch_size = batch_size

    def geocode(self,toponyms, context_toponyms):
        data = SentenceDataset(pd.DataFrame([[toponyms[i] + " " + context_toponyms[i],0] for i in range(len(toponyms))],columns=["sentence","label"]),self.tokenizer,batch_size=len(toponyms),shuffle=False)
        dataloader = DataLoader(data,  batch_size=self.batch_size)
        results = []
        for step, batch in enumerate(dataloader):
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            with torch.no_grad():
                outputs = self.bert_model(b_input_ids, 
                                token_type_ids=None, 
                                attention_mask=b_input_mask)
            results.append(outputs[0].detach().cpu().numpy())
        label = np.argmax(np.concatenate(results),axis=1)
        healpix_label = [self.label_healpix[l] for l in label]
        lat,lon = healpix2latlon(healpix_label,self.nside)
        return np.concatenate((lat.reshape(-1,1),lon.reshape(-1,1)),axis=1)