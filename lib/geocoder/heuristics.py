import pandas as pd
import numpy as np

from haversine import haversine_vector, Unit
from sklearn.cluster import DBSCAN

def heuristic_mean(geocoder,toponyms):
    input_ = np.asarray([[t1,t2] for t2 in toponyms for t1 in toponyms if t2 != t1])
    res_geocode = pd.DataFrame(input_,columns="t tc".split())
    lons,lats = geocoder.get_coords(input_[:,0],input_[:,1])
    res_geocode["lon"] = lons
    res_geocode["lat"] = lats
    results = {}
    for tp in toponyms:
        lat = res_geocode[res_geocode.t == tp].lat.mean()
        lon = res_geocode[res_geocode.t == tp].lon.mean()
        results[tp]={"lat":lat,"lon":lon}
    return results

def heuristic_no_context(geocoder,toponyms):
    input_ = np.asarray([[t1,t1] for t2 in toponyms for t1 in toponyms if t2 != t1])
    res_geocode = pd.DataFrame(input_,columns="t tc".split())
    lons,lats = geocoder.get_coords(input_[:,0],input_[:,1])
    res_geocode["lon"] = lons
    res_geocode["lat"] = lats
    results = {}
    for tp in toponyms:
        lat = res_geocode[res_geocode.t == tp].lat.mean()
        lon = res_geocode[res_geocode.t == tp].lon.mean()
        results[tp]={"lat":lat,"lon":lon}
    return results

def heuristic_cluster(geocoder,toponyms,eps=100):
    results = {}
    input_ = np.asarray([[t1,t2] for t2 in toponyms for t1 in toponyms if t2 != t1])
    res_geocode = pd.DataFrame(input_,columns="t tc".split())
    lons,lats = geocoder.get_coords(input_[:,0],input_[:,1])
    res_geocode["lon"] = lons
    res_geocode["lat"] = lats

    clf = DBSCAN(eps=eps)
    for t in toponyms:
        tp_df = res_geocode[res_geocode.tc == t].copy()

        coords = tp_df["lon lat".split()].values
        clf.fit(haversine_vector(coords,coords,unit="km",comb=True))

        tp_df["cluster"] = clf.labels_
        counts_ = dict(tp_df.cluster.value_counts())
        max_cluster = max(counts_, key=counts_.get)
        tp_df = tp_df[tp_df.cluster == max_cluster]
        lat = tp_df.lat.median()
        lon = tp_df.lon.median() #
        results[t]={"lat":lat,"lon":lon}
    return results