import numpy as np

from joblib import dump, load
from tensorflow.keras.utils import to_categorical

from lib.utils_geo import latlon2healpix
from lib.ngram_index import NgramIndex


def parse_bow(x,index):
    return np.sum(to_categorical(x,num_classes=index.cpt+1),axis=0)

def is_in(lat,lon,hp_predicted,hp_nside):
    hp_truth = latlon2healpix(lat,lon,hp_nside)
    return hp_truth == hp_predicted

class SVMGeocoder(object):
    
    def __init__(self,model_fn,ngram_index_filename):
        self.model = load(model_fn)
        self.ng_index = NgramIndex.load(ngram_index_filename)
    
    def geocode(self,phrase1,phrase2):
        if not phrase1 or not phrase2:
            return None
        vec = parse_bow(np.array(self.ng_index.encode(phrase1)),self.ng_index)+\
            parse_bow(np.array(self.ng_index.encode(phrase2)),self.ng_index)
        return self.model.predict([vec])[0]
    
    def geocode_multi(self,phrases1,phrases2):
        vecs = np.array([ parse_bow(np.array(self.ng_index.encode(ph)),self.ng_index) for ph in phrases1 if ph])
        vecs += np.array([ parse_bow(np.array(self.ng_index.encode(ph)),self.ng_index) for ph in phrases2 if ph])
        return self.model.predict(vecs)

hp = SVMGeocoder("SVMLINEAR_US_FR_AC.bin", "../../outputs/US_FR.txt_100_4_0.002__A_C_index")
hp.geocode("paris","montpellier")
