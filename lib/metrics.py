import tensorflow as tf

def lat_accuracy(LAT_TOL =1/180.):
    def accuracy_at_k_lat(y_true, y_pred):
        """
        Metrics use to measure the accuracy of the coordinate prediction. But in comparison to the normal accuracy metrics, we add a tolerance threshold due to the (quasi) impossible 
        task for neural network to obtain the exact  coordinate.

        Parameters
        ----------
        y_true : tf.Tensor
            truth data
        y_pred : tf.Tensor
            predicted output
        """
        diff = tf.abs(y_true - y_pred)
        fit = tf.dtypes.cast(tf.less(diff,LAT_TOL),tf.int64)
        return tf.reduce_sum(fit)/tf.size(y_pred,out_type=tf.dtypes.int64)
    return accuracy_at_k_lat

def lon_accuracy(LON_TOL=1/360.):
    def accuracy_at_k_lon(y_true, y_pred):
        """
        Metrics use to measure the accuracy of the coordinate prediction. But in comparison to the normal accuracy metrics, we add a tolerance threshold due to the (quasi) impossible 
        task for neural network to obtain the exact  coordinate.

        Parameters
        ----------
        y_true : tf.Tensor
            truth data
        y_pred : tf.Tensor
            predicted output
        """
        diff = tf.abs(y_true - y_pred)
        fit = tf.dtypes.cast(tf.less(diff,LON_TOL),tf.int64)
        return tf.reduce_sum(fit)/tf.size(y_pred,out_type=tf.dtypes.int64)
    return accuracy_at_k_lon