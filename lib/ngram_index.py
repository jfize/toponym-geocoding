import json

import numpy as np
import pandas as pd

from ngram import NGram
from transformers import BertTokenizer

# Machine learning 
from gensim.models import Word2Vec


class bertTokenizer:
    def __init__(self):
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased',do_lower_case=False)
    
    def split(self,string):
        return self.tokenizer.tokenize(string)

class NgramIndex():
    """
    Class used for encoding words in ngram representation
    """
    def __init__(self,n,bert_tokenization=False,loaded = False):
        """
        Constructor
        
        Parameters
        ----------
        n : int
            ngram size
        """
        self.ngram_gen = NGram(N=n)
        self.empty_char = "$"
        if bert_tokenization:
            self.ngram_gen = bertTokenizer()
            self.empty_char = "#"

        self.size = n
        self.ngram_index = {"":0}
        self.index_ngram = {0:""}

        self.freq_ngram = {}
        self.cpt = 0
        self.max_len = 0

        self.loaded = loaded

        
    def split_and_add(self,word):
        """
        Split word in multiple ngram and add each one of them to the index
        
        Parameters
        ----------
        word : str
            a word
        """
        ngrams = str(word).lower().replace(" ",self.empty_char)
        ngrams = list(self.ngram_gen.split(ngrams))
        [self.add(ngram) for ngram in ngrams if not ngram in self.ngram_index]
        self.max_len = max(self.max_len,len(ngrams))

    def add(self,ngram):
        """
        Add a ngram to the index
        
        Parameters
        ----------
        ngram : str
            ngram
        """
        if not ngram in self.ngram_index:
            self.cpt+=1
            self.ngram_index[ngram]=self.cpt
            self.index_ngram[self.cpt]=ngram
            self.freq_ngram[ngram] = 1
        else:
            self.freq_ngram[ngram] += 1
        
    def filter_ngram_by_freq(self,threshold=20):
        freq_data = pd.DataFrame(self.freq_ngram.items(),columns="ngram freq".split())
        selected_ngram = freq_data[freq_data.freq>threshold]
        selected_ngram["index__"] = np.arange(len(selected_ngram))
        self.ngram_index = dict(selected_ngram["ngram index__".split()].values)
        self.index_ngram = dict(selected_ngram["index__ ngram".split()].values)
    
    def filter_top_ngram(self,threshold=20000):
        freq_data = pd.DataFrame(self.freq_ngram.items(),columns="ngram freq".split()).sort_values(by="freq",ascending=False)
        if len(self.ngram_index)-threshold <=0:
            return 0 
        selected_ngram = freq_data.head(threshold)
        selected_ngram["index__"] = np.arange(len(selected_ngram))
        self.ngram_index = dict(selected_ngram["ngram index__".split()].values)
        self.index_ngram = dict(selected_ngram["index__ ngram".split()].values)

        
        

    def encode(self,word,complete=True):
        """
        Return a ngram representation of a word
        
        Parameters
        ----------
        word : str
            a word
        
        Returns
        -------
        list of int
            listfrom shapely.geometry import Point,box
 of ngram index
        """
        ngrams = str(word).lower().replace(" ",self.empty_char)
        ngrams = list(self.ngram_gen.split(ngrams))
        ngrams = [ng for ng in ngrams if ng.count(self.empty_char)<2]
        if not complete:
            return [self.ngram_index[ng] for ng in ngrams if ng in self.ngram_index]
        return self.complete([self.ngram_index[ng] for ng in ngrams if ng in self.ngram_index],self.max_len)

    def complete(self,ngram_encoding,MAX_LEN,filling_item=0):
        """
        Complete a ngram encoded version of word with void ngram. It's necessary for neural network.
        
        Parameters
        ----------
        ngram_encoding : list of int
            first encoding of a word
        MAX_LEN : int
            desired length of the encoding
        filling_item : int, optional
            ngram index you wish to use, by default 0
        
        Returns
        -------
        list of int
            list of ngram index
        """
        if self.loaded and len(ngram_encoding) >=MAX_LEN:
            return ngram_encoding[:MAX_LEN]
        assert len(ngram_encoding) <= MAX_LEN
        diff = MAX_LEN - len(ngram_encoding)
        ngram_encoding.extend([filling_item]*diff)  
        return ngram_encoding
    
    def get_embedding_layer(self,texts,dim=100,**kwargs):
        """
        Return an embedding matrix for each ngram using encoded texts. Using gensim.Word2vec model.
        
        Parameters
        ----------
        texts : list of [list of int]
            list of encoded word
        dim : int, optional
            embedding dimension, by default 100
        
        Returns
        -------
        np.array
            embedding matrix
        """
        sentences = SentenceIterator(self,texts,True)
        model = Word2Vec(sentences, size=dim,window=5, min_count=1, workers=4,**kwargs)
        N = len(self.ngram_index)
        embedding_matrix = np.zeros((N,dim))
        for i in range(N):
            if str(i) in model.wv:
                embedding_matrix[i] = model.wv[str(i)]
        return embedding_matrix

    def get_glove_embedding_layer(self,texts,dim=100,**kwargs):
        """
        Return an embedding matrix for each ngram using encoded texts. Using gensim.Word2vec model.
        
        Parameters
        ----------
        texts : list of [list of int]
            list of encoded word
        dim : int, optional
            embedding dimension, by default 100
        
        Returns
        -------
        np.array
            embedding matrix
        """
        from glove import Corpus, Glove
        corpus = Corpus()
        corpus.fit([[str(w) for w in t] for t in texts], window=10)
        glove = Glove(no_components=dim, learning_rate=0.05)
        glove.fit(corpus.matrix, epochs=30, no_threads=4, verbose=True)
        glove.add_dictionary(corpus.dictionary)
        N = len(self.ngram_index)
        embedding_matrix = np.zeros((N,dim))
        for i in range(N):
            if str(i) in glove.dictionary:
                embedding_matrix[i] = glove.word_vectors[glove.dictionary[str(i)]]
        return embedding_matrix
        

    def save(self,fn):
        """

        Save the NgramIndex
        
        Parameters
        ----------
        fn : str
            output filename
        """
        data = {
            "ngram_size": self.size,
            "ngram_index": self.ngram_index,
            "cpt_state": self.cpt,
            "max_len_state": self.max_len
        }
        json.dump(data,open(fn,'w'))

    @staticmethod
    def load(fn):
        """
        
        Load a NgramIndex state from a file.
        
        Parameters
        ----------
        fn : str
            input filename
        
        Returns
        -------
        NgramIndex
            ngram index
        
        Raises
        ------
        KeyError
            raised if a required field does not appear in the input file
        """
        try:
            data = json.load(open(fn))
        except json.JSONDecodeError:
            print("Data file must be a JSON")
        for key in ["ngram_size","ngram_index","cpt_state","max_len_state"]:
            if not key in data:
                raise KeyError("{0} field cannot be found in given file".format(key))
        new_obj = NgramIndex(data["ngram_size"],loaded=True)
        new_obj.ngram_index = data["ngram_index"]
        new_obj.index_ngram = {v:k for k,v in new_obj.ngram_index.items()}
        new_obj.cpt = data["cpt_state"]
        new_obj.max_len = data["max_len_state"]
        return new_obj

class SentenceIterator:

    """Iterator that counts upward forever."""

    def __init__(self, ng_encoder, input_,cast_str = False):
        self.ng_encoder = ng_encoder
        self.input = input_
        self.i = -1
        self.cast_str = cast_str

    def __iter__(self):
        self.i = -1
        return self
    
    def __len__(self):
        return len(self.input)

    def __next__(self):
        try:
            self.i +=1
            if not self.cast_str:
                return self.ng_encoder.encode(self.input[self.i])
            else:
                return list(map(str,self.ng_encoder.encode(self.input[self.i])))
        except:
            raise StopIteration
