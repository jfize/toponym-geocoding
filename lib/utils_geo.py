
import geopandas as gpd
import numpy as np
import pandas as pd

from shapely.geometry import Point,box 
import healpy

from tqdm import tqdm


import pandas as pd, numpy as np
from numba import njit
from .helpers import read_geonames
from tqdm import tqdm
from joblib import Parallel,delayed

import tensorflow as tf
import keras.backend as K

def tf_deg2rad(deg):
    pi_on_180 = 0.017453292519943295
    return deg * pi_on_180

# convert lat and lon to a healpix code encoding a region, with a given resolution
def latlon2healpix( lat , lon , res ):
    lat = np.radians(lat)
    lon = np.radians(lon)
    xs = ( np.cos(lat) * np.cos(lon) ) #
    ys = ( np.cos(lat) * np.sin(lon) ) # -> Sphere coordinates: https://vvvv.org/blog/polar-spherical-and-geographic-coordinates
    zs = ( np.sin(lat) ) # 
    return healpy.vec2pix( int(res) , xs , ys , zs )

def healpix2latlon( code , nside ):
    xs, ys, zs = healpy.pix2vec( nside , code )
    lat =  np.arctan2(zs, np.sqrt(xs * xs + ys * ys)) * 180.0 / np.pi 
    lon =  np.arctan2(ys, xs) * 180.0 / np.pi 
    return lat, lon

def haversine_tf(y_true,y_pred):
    """
    Return the geodesic distance between (lon1,lat1) and (lon2,lat2) coordinates
    
    Parameters
    ----------
    lon1 : numeric or array-like (pandas Dataframe works also)
        longitude of first coordinates
    lat1 :  numeric or array-like (pandas Dataframe works also)
        latitude of first coordinates
    lon2 :  numeric or array-like (pandas Dataframe works also)
        longitude of second coordinates
    lat2 :  numeric or array-like (pandas Dataframe works also)
        longitude of second coordinates
    
    Returns
    -------
    float or array-like
        distance(s) value(s)
    """
    lon1, lat1, lon2, lat2 = map(tf_deg2rad, [y_true[:,0], y_true[:,1], y_pred[:,0], y_pred[:,1]])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = K.sin(dlat/2.0)**2 + K.cos(lat1) * K.cos(lat2) * K.sin(dlon/2.0)**2
    
    return 6367 * 2 * tf.math.asin(K.sqrt(a))

def haversine_tf_1circle(y_true,y_pred):
    """
    Return the geodesic distance between (lon1,lat1) and (lon2,lat2) coordinates
    
    Parameters
    ----------
    lon1 : numeric or array-like (pandas Dataframe works also)
        longitude of first coordinates
    lat1 :  numeric or array-like (pandas Dataframe works also)
        latitude of first coordinates
    lon2 :  numeric or array-like (pandas Dataframe works also)
        longitude of second coordinates
    lat2 :  numeric or array-like (pandas Dataframe works also)
        longitude of second coordinates
    
    Returns
    -------
    float or array-like
        distance(s) value(s)
    """
    lon1, lat1, lon2, lat2 = map(tf_deg2rad, [y_true[:,0], y_true[:,1], y_pred[:,0], y_pred[:,1]])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = K.sin(dlat/2.0)**2 + K.cos(lat1) * K.cos(lat2) * K.sin(dlon/2.0)**2
    
    return 1 * 2 * tf.math.asin(K.sqrt(a))

def to_wgs84_lat(lat):
    return ((lat*180)-90)
def to_wgs84_lon(lon):
    return ((lon*360)-180)

def to_wgs84(x):
    lon=to_wgs84_lon(x[:,0])
    lat=to_wgs84_lat(x[:,1])
    return tf.stack([lon,lat],axis=1)

def accuracy_k(k=100):#km
    def compute_metric(y_true,y_pred):
        return K.less_equal(haversine_tf(to_wgs84(y_true),to_wgs84(y_pred)),k) 
    return compute_metric

def haversine_pd(lon1, lat1, lon2, lat2):
    """
    Return the geodesic distance between (lon1,lat1) and (lon2,lat2) coordinates
    
    Parameters
    ----------
    lon1 : numeric or array-like (pandas Dataframe works also)
        longitude of first coordinates
    lat1 :  numeric or array-like (pandas Dataframe works also)
        latitude of first coordinates
    lon2 :  numeric or array-like (pandas Dataframe works also)
        longitude of second coordinates
    lat2 :  numeric or array-like (pandas Dataframe works also)
        longitude of second coordinates
    
    Returns
    -------
    float or array-like
        distance(s) value(s)
    """
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat/2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2.0)**2
    
    return 6367 * 2 * np.arcsin(np.sqrt(a))


def get_adjacent(ids,lon1, lat1, lon2, lat2,threshold):
    dist_ = haversine_pd(lon1, lat1, lon2, lat2)
    return ids[dist_<threshold]

def get_geonames_adjacency(geoname_data,threshold):
    return Parallel(n_jobs=-1,backend="multiprocessing")(delayed(get_adjacent)(geoname_data.geonameid.values,
    geoname_data.longitude,
    geoname_data.latitude,
    row.longitude,
    row.latitude,
    threshold) for ix,row in tqdm(geoname_data.iterrows(),total=len(geoname_data)))


def generate_couple(object_list):
    """
    Return a randomly selected couple from an object list.
    
    Parameters
    ----------
    object_list : list
        object list
    
    Returns
    -------
    list
        list of coupled object
    """
    couples = []
    lst = np.arange(len(object_list))
    for _ in range(len(object_list)):
        if len(lst) == 1:
            break
        idx = np.random.choice(np.arange(len(lst)))
        idx2 = np.random.choice(np.arange(len(lst)))
        while idx2 == idx:
            idx2 = np.random.choice(np.arange(len(lst)))
        couples.append([object_list[lst[idx]],object_list[lst[idx2]]])
        lst = np.delete(lst,idx)
    return couples

def _hash_couple(o1,o2):
    """
    Return an hash for two object ids.
    
    Parameters
    ----------
    o1 : str or int
        id of the first objeeect
    o2 : str of int
        id of the second object
    
    Returns
    -------
    str
        hash
    """
    return "|".join(map(str,sorted([int(o1),int(o2)])))



def zero_one_encoding(long,lat):
    """
    Encode coordinates (WGS84) between 0 and 1
    
    Parameters
    ----------
    long : float
        longitude value
    lat : float
        latitude value
    
    Returns
    -------
    float,float
        longitude, latitude
    """
    return ((long + 180.0 ) / 360.0), ((lat + 90.0 ) / 180.0) 

class Cell(object):
    """
    A cell is box placed in geeographical space.
    """
    def __init__(self,upperleft_x,upperleft_y,bottomright_x,bottomright_y,x,y):
        """
        Constructor
        
        Parameters
        ----------
        upperleft_x : float
            upperleft longitude
        upperleft_y : float
            upperleft latitude
        bottomright_x : float
            bottom right longitude
        bottomright_y : float
            bottom right latitude
        x : int
            cell x coordinates in the grid
        y : int
            cell y coordinates in the grid
        """
        self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y = upperleft_x,upperleft_y,bottomright_x,bottomright_y
        self.box_ = box(self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y)
        self.list_object={} # {id:Point(coord)}

        self.x,self.y = x, y

    def contains(self,lat,lon):
        """
        Return true if the cell contains a point at given coordinates
        
        Parameters
        ----------
        lat : float
            latitude
        lon : float
            longitude
        
        Returns
        -------
        bool
            true if contains
        """ 
        x,y = lon,lat
        if x < self.upperleft_x or x > self.bottomright_x:
            return False
        if y < self.upperleft_y or y > self.bottomright_y:
            return False
        return True
    
    def add_object(self,id_,lat,lon):
        """
        Connect an object to the cell
        
        Parameters
        ----------
        id_ : int
            id
        lat : float
            latitude
        lon : float
            longitude
        """
        self.list_object[id_] = Point(lon,lat)
            
    def __repr__(self):
        return  "upperleft:{0}_{1}_;bottom_right:{2}_{3}".format(self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y)
    
        
class Grid(object):
    """
    Define a grid 
    
    """
    def __init__(self,upperleft_x,upperleft_y,bottomright_x,bottomright_y,cell_sub_div_index=[100,50]):
        """
        Constructor
        
        Parameters
        ----------
        upperleft_x : float
            upperleft longitude
        upperleft_y : float
            upperleft latitude
        bottomright_x : float
            bottom right longitude
        bottomright_y : float
            bottom right latitude
        cell_sub_div_index : list, optional
            number of division in both latitude and longitude axis (longitude first), by default [100,50]
        """
        self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y = upperleft_x,upperleft_y,bottomright_x,bottomright_y
        
        self.x_r = abs(self.bottomright_x - self.upperleft_x)/cell_sub_div_index[0]
        self.y_r = abs(self.upperleft_y - self.bottomright_y )/cell_sub_div_index[1]
        
        self.c_x_r = self.x_r/cell_sub_div_index[0] # Redivide
        self.c_y_r = self.y_r/cell_sub_div_index[1]
        
        self.cells = []
        self.inter_cells = []
        for i in range(cell_sub_div_index[1]):
            self.cells.append([])
            for j in range(cell_sub_div_index[0]):
                self.cells[-1].append(Cell(
                    self.upperleft_x+j*self.x_r,
                    self.upperleft_y+i*self.y_r,
                    self.upperleft_x+((j+1)*self.x_r),
                    self.upperleft_y+((i+1)*self.y_r),
                    j,i)
                )
        dec_y = 0 
        for i in range(cell_sub_div_index[1]):
            self.inter_cells.append([])
            dec_x = 0 
            for j in range(cell_sub_div_index[0]):                 
                self.inter_cells[-1].append(Cell(
                    self.upperleft_x+(j*self.x_r)-self.c_x_r, # TOP
                    self.upperleft_y+(i*self.y_r)-dec_y,
                    self.upperleft_x+((j+1)*self.x_r)-self.c_x_r,#(self.u_pos*self.c_x_r),
                    self.upperleft_y+((i+1)*self.y_r)+self.c_y_r,
                    j,i)
                )
                self.inter_cells[-1].append(Cell(
                    self.upperleft_x+(j*self.x_r)-self.c_x_r, # CENTER
                    self.upperleft_y+(i*self.y_r)-self.c_y_r,
                    self.upperleft_x+((j+1)*self.x_r)+self.c_x_r,
                    self.upperleft_y+((i+1)*self.y_r)+self.c_y_r,
                    j,i)
                )
                self.inter_cells[-1].append(Cell(
                    self.upperleft_x+(j*self.x_r)+dec_x, # CENTER
                    self.upperleft_y+(i*self.y_r)-self.c_y_r,
                    self.upperleft_x+((j+1)*self.x_r)-self.c_x_r, #LEFT
                    self.upperleft_y+((i+1)*self.y_r)+self.c_y_r,
                    j,i)
                )
                dec_x = self.c_x_r
            dec_y = self.c_y_r
    
    def fit_data(self,data = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))):
        """
        
        To avoid unnecessary check when connecting an entity to one or multiple cells, we 
        filter cells that does not appears in our geographic context (here countries surface).
        
        Parameters
        ----------
        data : GeoDataFrame
            geographic context
        """
        world = data 
        world["nn"] = 1
        dissolved = world.dissolve(by="nn").iloc[0].geometry
        new_cells= []
        new_inter_cells=[]
        for i in tqdm(range(len(self.cells))):
            for j in range(len(self.cells[i])):
                if dissolved.intersects(self.cells[i][j].box_):
                    new_cells.append(self.cells[i][j])
                    new_inter_cells.extend(self.inter_cells[i][j*3:(j+1)*3])
                    
        self.cells=new_cells
        self.inter_cells = new_inter_cells
        
                    
    def __add__(self,a): 
        """
        Add an object to the grid
        
        Parameters
        ----------
        a : tuple
            (id, latitude, longitude)
        """
        for c1 in range(len(self.cells)):
            if self.cells[c1].contains(a[1],a[2]):
                self.cells[c1].add_object(*a)
                
        for c1 in range(len(self.inter_cells)):
            if self.inter_cells[c1].contains(a[1],a[2]):
                self.inter_cells[c1].add_object(*a)
                
    def get_adjacent_relationships(self,random_iteration=10):
        """
        Return a list of adjacent relationships founds in each cell.
        
        Parameters
        ----------
        random_iteration : int, optional
            number of iteration for random selection of adjacency relationships, by default 10
        
        Returns
        -------
        list
            adjacency relationships
        """
        relationships = set([])
        for c1 in tqdm(range(len(self.cells))):
            for _ in range(random_iteration):
                for t in generate_couple(list(self.cells[c1].list_object.keys())):
                    relationships.add(_hash_couple(t[0],t[1]))

        for c1 in tqdm(range(len(self.inter_cells))):
            for _ in range(random_iteration):
                for t in generate_couple(list(self.inter_cells[c1].list_object.keys())):
                    relationships.add(_hash_couple(t[0],t[1]))
        return relationships
    


def get_adjacency_rels(geodataframe,bounds,subdiv_tuple,random_iter_adjacency):
    g = Grid(*bounds,subdiv_tuple)
    g.fit_data()
    [g+(int(row.geonameid),row.latitude,row.longitude) for ix,row in tqdm(geodataframe["geonameid longitude latitude".split()].iterrows(),total=len(geodataframe))]
    return [[int(i) for i in r.split("|")] for r in g.get_adjacent_relationships(random_iter_adjacency)]

def get_geonames_inclusion_rel(geonames_data,geonames_hierarchy_data_fn):
    geonames_hierarchy_data = pd.read_csv(geonames_hierarchy_data_fn,sep="\t",header=None,names="parentId,childId,type".split(",")).fillna("")
    geonamesIDS = set(geonames_data.geonameid.values)
    filter_mask = (geonames_hierarchy_data.childId.isin(geonamesIDS) & geonames_hierarchy_data.parentId.isin(geonamesIDS))
    return (geonames_hierarchy_data[filter_mask]["childId parentId".split()].values.tolist())

def get_bounds(geodataframe):
    geodataframe["geometry"] = geodataframe["longitude latitude".split()].apply(lambda x: Point(x.longitude,x.latitude),axis=1)
    geodataframe = gpd.GeoDataFrame(geodataframe)
    geodataframe["i"]=1
    return geodataframe.dissolve("i").bounds.values[0] # Required to get adjacency relationships
