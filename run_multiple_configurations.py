from lib.run import GridSearchModel
from collections import OrderedDict
c_f = "--wikipedia-cooc-fn ../data/wikipedia/cooccurrence_FR.txt"

# Init GridsearchModel
grid = GridSearchModel(\
    "python3 combination_embeddingsv3inverse.py",
    **OrderedDict({ # necessary because some args have to be given in a certain order
    "rel":["-w "+c_f,("-i -w "+c_f),"-a -w "+c_f,"-a -i -w "+c_f], # ,"-a -i -w "+c_f ,"-i -a"
    "-n":[4],
    "--ngram-word2vec-iter" :[100],
    "-e":[100],
    "geoname_fn":"../data/geonamesData/FR.txt".split(),
    "hierarchy_fn":"../data/geonamesData/hierarchy.txt".split()
    }.items()))

print("########### THE FOLLOWING COMMAND(S) WILL BE EXECUTED ###########" )
[print(task.get_command()) for task in grid.tasks]
print("#################################################################")
grid.run("outputs/log_{0}".format("FR_model_v2"))

#["-w --wikipedia-cooc-fn ../data/wikipedia/cooccurrence_FR.txt","-w --wikipedia-cooc-fn ../data/wikipedia/cooccurrence_FR.txt -a","-w --wikipedia-cooc-fn ../data/wikipedia/cooccurrence_FR.txt -i"]