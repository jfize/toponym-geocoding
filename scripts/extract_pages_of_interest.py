import json
import gzip
import argparse

from joblib import Parallel, delayed

# To avoid progressbar issue
from tqdm import tqdm



parser = argparse.ArgumentParser()
parser.add_argument("wikidata_json_dump_filename",help="Wikipedia JSON dump compressed with gzip (*.gz)")
parser.add_argument("output_filename")

args = parser.parse_args()

# Prepare Output File
output = open(args.output_filename,'w')
output.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n".format("ID_WIKIDATA","title","url","latitude","longitude","classes"))

def job(line):
    line = line.decode("utf-8")

    if not "\"P625\"" in line or not "\"P31\"" in line:
        return
    try: 
        data = json.loads(line.strip(",\n"))
        if "sitelinks" in data and "claims" in data:
            if "enwiki" in data["sitelinks"] or "frwiki"  in data["sitelinks"]:
                page_available = [i for i in ["en","fr"] if i+"wiki" in data["sitelinks"]]
                for site in page_available:
                    site = 'en' if 'enwiki' in data["sitelinks"] else 'fr'
                    id_ = data["id"]
                    coords_data = data["claims"]["P625"][0]["mainsnak"]["datavalue"]["value"]
                    title = data["sitelinks"]["{0}wiki".format(site)]["title"] 
                    url = "https://{1}.wikipedia.org/wiki/{0}".format(title.replace(" ","_"),site)
                    lat = coords_data["latitude"]
                    lon = coords_data["longitude"]
                    classes_ = ""
                    for claimP31 in data["claims"]["P31"]:
                        classes_ = classes_ + "_"+ str(claimP31["mainsnak"]["datavalue"]["value"]["id"])
                    output.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\n".format(id_,title,url,lat,lon,classes_.strip("_")))
    except Exception: # First Line is "['" and last line is "]'"
        pass


Parallel(n_jobs=8,backend="multiprocessing")(delayed(job)(line)for line in tqdm(gzip.GzipFile(args.wikidata_json_dump_filename),unit_scale=True,unit_divisor=1000))
