import gzip
import json
import re

import argparse

import pandas as pd

from joblib import Parallel,delayed
from tqdm import tqdm

parser = argparse.ArgumentParser()

parser.add_argument("page_of_interest_fn")
parser.add_argument("output_fn")
parser.add_argument("-c","--corpus",action="append")

args = parser.parse_args()#("../wikidata/sample/place_en_fr_page_clean_onlyfrplace.csv test.txt -c frwiki-latest.json.gz -c enwiki-latest.json.gz".split())

PAGES_OF_INTEREST_FILE = args.page_of_interest_fn
WIKIPEDIA_CORPORA = args.corpus
OUTPUT_FN = args.output_fn

if len(WIKIPEDIA_CORPORA)<1:
    raise Exception('No corpora was given!')

df = pd.read_csv(PAGES_OF_INTEREST_FILE)
page_of_interest = set(df.title.values)

page_coord = {row.title : (row.longitude,row.latitude) for ix,row in df.iterrows()}

output = open(OUTPUT_FN,'w')
output.write("title\tinterlinks\tlongitude\tlatitude\n")
for wikipedia_corpus in WIKIPEDIA_CORPORA:
    for line in tqdm(gzip.GzipFile(wikipedia_corpus,'rb')):
        data = json.loads(line)
        if data["title"] in page_of_interest:
            occ = page_of_interest.intersection(data["interlinks"].keys())
            coord = page_coord[data["title"]]
            if len(occ) >0:output.write(data["title"]+"\t"+"|".join(occ)+"\t{0}\t{1}".format(*coord)+"\n")
